1. Vytvořte třídu, která **načte soubor** `assets/ads.json` a **převede** jej na formát `assets/ads.txt`

2. Vytvořte třídu, která **zvaliduje** `assets/ads.txt`. Nevalidní řádky ze souboru `assets/ads.txt` odstraňte. Pro soubor platí následující pravidla
    - `Field #1` - musí být validní URL
    - `Field #2` - validovat že se nejedná o prázdný string
    - `Field #3` - obsahuje jednu z hodnot “RESELLER” nebo “DIRECT”, nezáleží na velikosti písmen
    - `Field #4` - nepovinné pole, není třeba validovat

3. Vytvořte třídu pro **načtení vzdáleného souboru** ads.txt (vzorový soubor je na adrese https://trackad.cz/ads.txt). Při parsování tohoto souboru ignorovat prázdné řádky a komentáře (řádky začínající symbolem #). Při načítá ošetřit následující případy:
    - Server je nedostupný
    - Na serveru neexistuje soubor ads.txt
    - Zvýšená doba odezvy - nastavit dobu timeoutu na 5s

4. Vytvořte třídu pro **porovnání dvou ads.txt souborů** (první soubor je označen jako lokální, druhý soubor je označen jako vzdálený). Třída bude obsahovat následující metody:
    - Metodu vracející řádky lokálního souboru, které se nenachází ve vzdáleném
    - Pro každou jedinečnou url v lokálním souboru, najde další záznamy ve vzdáleném souboru a přidat je do existujícího lokálního souboru

5. Vytvořit *script/konzolový task*, který bude přijímat následující parametry:
    - `cestu k lokálnímu souboru`
    - `url adresa vzdáleného ads.txt`
Script provede následující kroky
    - Načte lokální soubor, pro načtení použijte třídu z bodu 1.
    - Provede validaci pomocí třídy z bodu 2.
    - Načte vzdálený soubor pomocí třídy z bodu 3.
    - Vytvoří soubor s názvem `invalid-ads.txt`.V tomto souboru budou vypsány záznamy vrácené metodou 4a ve formátu ads.txt. Na prvním řádku toho souboru bude v komentář (řádek začíná #) uvedeno “this file was generated at: DATE” kde DATE bude čas vytvoření souboru ve formátu `d.m.Y. H:i:s`
    - Vytvoří nový soubor s názevm ads.txt, který bude obsahovat jen validní záznamy lokálního souboru + nové záznamy které vrátí metodu popsaná ve 4b. Záznamy budou primárně řazeny podle Field #1 a sekundárně podle Field #2
Script musí vypisovat log co právě dělá. Tento log se ukládá i do souboru. Musí být ošetřeny všechny chybové stavy, task nesmí skončit chybou.

**Formát ads.txt:**
 - `<Field #1>, <Field #2>, <Field #3>, <Field #4>`
 - Field #4 je nepovinný parametr
 - příklad: foo.example.com, 54795, DIRECT, ""

